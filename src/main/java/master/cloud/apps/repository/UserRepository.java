package master.cloud.apps.repository;

import master.cloud.apps.model.User;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByNick(String nick);

    boolean existsByNick(String nick);

}
