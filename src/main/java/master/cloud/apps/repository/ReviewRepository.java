package master.cloud.apps.repository;

import master.cloud.apps.model.Review;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewRepository extends JpaRepository<Review, Long> {

    Collection<Review> findByUserId(Long id);

    Optional<Review> findByBookIdAndId(Long bookId, Long commentId);

}
