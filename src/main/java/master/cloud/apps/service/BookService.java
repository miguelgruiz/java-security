package master.cloud.apps.service;

import master.cloud.apps.dto.request.BookRequestDto;
import master.cloud.apps.dto.request.ReviewRequestDto;
import master.cloud.apps.dto.request.UpdateBookRequestDto;
import master.cloud.apps.dto.response.BookResponseDto;
import master.cloud.apps.dto.response.BookDetailsResponseDto;
import master.cloud.apps.dto.response.ReviewResponseDto;
import master.cloud.apps.dto.response.UserResponseDto;
import master.cloud.apps.exceptions.BookNotFoundException;
import master.cloud.apps.exceptions.UserCanNotBeDeletedException;
import master.cloud.apps.exceptions.UserNotFoundException;
import master.cloud.apps.model.Book;
import master.cloud.apps.model.Review;
import master.cloud.apps.model.User;
import master.cloud.apps.repository.BookRepository;
import master.cloud.apps.repository.ReviewRepository;
import master.cloud.apps.repository.UserRepository;
import java.util.Collection;
import java.util.stream.Collectors;

import org.dozer.Mapper;
import org.springframework.stereotype.Service;

@Service
public class BookService {

    BookRepository bookRepository;

    ReviewRepository reviewRepository;

    UserRepository userRepository;

    Mapper mapper;

    public BookService(Mapper mapper, BookRepository bookRepository, ReviewRepository reviewRepository, UserRepository userRepository) {
        this.mapper = mapper;
        this.bookRepository = bookRepository;
        this.reviewRepository = reviewRepository;
        this.userRepository = userRepository;
    }

    public Collection<BookResponseDto> findAll() {
        return this.bookRepository.findAll().stream()
                .map(book -> this.mapper.map(book, BookResponseDto.class))
                .collect(Collectors.toList());
    }

    public BookDetailsResponseDto findById(Long id) {
        Book book = this.bookRepository.findById(id).orElseThrow(BookNotFoundException::new);
        return this.mapper.map(book, BookDetailsResponseDto.class);
    }

    public BookDetailsResponseDto save(BookRequestDto bookRequestDto) {
        Book book = this.mapper.map(bookRequestDto, Book.class);
        book = this.bookRepository.save(book);
        return this.mapper.map(book, BookDetailsResponseDto.class);
    }

    public BookDetailsResponseDto update(Long id, UpdateBookRequestDto updateBookRequestDto) {
        Book book = this.bookRepository.findById(id).orElseThrow(BookNotFoundException::new);
        book = updateBookValues(book, updateBookRequestDto);
        book = this.bookRepository.save(book);
        return this.mapper.map(book, BookDetailsResponseDto.class);

    }

    public BookDetailsResponseDto delete(Long id) {
        Book book = this.bookRepository.findById(id).orElseThrow(BookNotFoundException::new);
        this.bookRepository.delete(book);
        return this.mapper.map(book, BookDetailsResponseDto.class);

    }

    public ReviewResponseDto addReview(long id, ReviewRequestDto reviewRequestDto) {
        Book book = this.bookRepository.findById(id).orElseThrow(BookNotFoundException::new);
        User user = this.userRepository.findByNick(reviewRequestDto.getNick()).orElseThrow(UserNotFoundException::new);
        Review review = this.mapper.map(reviewRequestDto, Review.class);
        review.setBook(book);
        review.setUser(user);
        review = this.reviewRepository.save(review);
        return this.mapper.map(review, ReviewResponseDto.class);
    }

    private Book updateBookValues(Book book, UpdateBookRequestDto updateBookRequestDto) {
        if(updateBookRequestDto.getTitle() != null && !updateBookRequestDto.getTitle().isEmpty()) {
            book.setTitle(updateBookRequestDto.getTitle());
        }
        if(updateBookRequestDto.getAuthor() != null && !updateBookRequestDto.getAuthor().isEmpty()) {
            book.setAuthor(updateBookRequestDto.getAuthor());
        }
        if(updateBookRequestDto.getPublisher() != null && !updateBookRequestDto.getPublisher().isEmpty()) {
            book.setPublisher(updateBookRequestDto.getPublisher());
        }
        if(updateBookRequestDto.getSummary() != null && !updateBookRequestDto.getSummary().isEmpty()) {
            book.setSummary(updateBookRequestDto.getSummary());
        }
        if(updateBookRequestDto.getYear() > 0) {
            book.setYear(updateBookRequestDto.getYear());
        }

        return book;
    }

}
