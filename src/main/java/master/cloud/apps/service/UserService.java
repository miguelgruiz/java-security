package master.cloud.apps.service;

import master.cloud.apps.dto.request.UpdateUserEmailRequestDto;
import master.cloud.apps.dto.request.UserRequestDto;
import master.cloud.apps.dto.response.UserResponseDto;
import master.cloud.apps.exceptions.UserCanNotBeDeletedException;
import master.cloud.apps.exceptions.UserNotFoundException;
import master.cloud.apps.exceptions.UserWithSameNickException;
import master.cloud.apps.model.User;
import master.cloud.apps.repository.UserRepository;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import static java.util.Collections.emptyList;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    Mapper mapper;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    public Collection<UserResponseDto> findAll() {
        return this.userRepository.findAll().stream()
                .map(user -> this.mapper.map(user, UserResponseDto.class))
                .collect(Collectors.toList());
    }

    public UserResponseDto findById(Long id) {
        User user = this.userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        return this.mapper.map(user, UserResponseDto.class);
    }

    public UserResponseDto save(UserRequestDto userRequestDto) {
        if (this.userRepository.existsByNick(userRequestDto.getNick())) {
            throw new UserWithSameNickException();
        }
        User user = this.mapper.map(userRequestDto, User.class);
        user.setPassword(bCryptPasswordEncoder.encode(userRequestDto.getPassword()));
        user = this.userRepository.save(user);
        return this.mapper.map(user, UserResponseDto.class);
    }

    public UserResponseDto updateEmail(Long id, UpdateUserEmailRequestDto updateUserEmailRequestDto) {
        User user = this.userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        if (!user.getEmail().equalsIgnoreCase(updateUserEmailRequestDto.getEmail())) {
            user.setEmail(updateUserEmailRequestDto.getEmail());
            user = this.userRepository.save(user);
        }
        return this.mapper.map(user, UserResponseDto.class);
    }

    public UserResponseDto delete(Long id) {
        User user = this.userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        if (!user.getReviews().isEmpty()) {
            throw new UserCanNotBeDeletedException();
        }
        this.userRepository.delete(user);
        return this.mapper.map(user, UserResponseDto.class);

    }

    public User findByNick(String nick) {
        Optional<User> user = userRepository.findByNick(nick);
        if (!user.isPresent()) {
            return null;
        }
        return user.get();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByNick(username);
        if (!user.isPresent()) {
            throw new UsernameNotFoundException(username);
        }
        return new org.springframework.security.core.userdetails.User(user.get().getNick(), user.get().getPassword(), emptyList());
    }

}
