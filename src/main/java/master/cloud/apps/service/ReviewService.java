package master.cloud.apps.service;

import master.cloud.apps.dto.response.ReviewResponseDto;
import master.cloud.apps.dto.response.UserReviewResponseDto;
import master.cloud.apps.exceptions.CommentNotFoundException;
import master.cloud.apps.model.Review;
import master.cloud.apps.repository.ReviewRepository;
import master.cloud.apps.repository.UserRepository;
import java.util.Collection;
import java.util.stream.Collectors;

import org.dozer.Mapper;
import org.springframework.stereotype.Service;

@Service
public class ReviewService {

    ReviewRepository reviewRepository;

    UserRepository userRepository;

    private Mapper mapper;

    public ReviewService(Mapper mapper, ReviewRepository reviewRepository, UserRepository userRepository) {
        this.mapper = mapper;
        this.reviewRepository = reviewRepository;
        this.userRepository = userRepository;
    }

    public Collection<UserReviewResponseDto> getReviewsFromUser(Long id) {
        return this.reviewRepository.findByUserId(id).stream()
                .map(review -> this.mapper.map(review, UserReviewResponseDto.class))
                .collect(Collectors.toList());

    }

    public ReviewResponseDto delete(Long bookId, Long reviewId) {
        Review review = reviewRepository.findByBookIdAndId(bookId, reviewId).orElseThrow(CommentNotFoundException::new);
        reviewRepository.delete(review);

        return this.mapper.map(review, ReviewResponseDto.class);
    }

}
