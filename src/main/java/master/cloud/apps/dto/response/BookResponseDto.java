package master.cloud.apps.dto.response;

import lombok.Data;

@Data
public class BookResponseDto {

    private Long id;
    private String title;

}
