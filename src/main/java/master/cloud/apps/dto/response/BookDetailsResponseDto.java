package master.cloud.apps.dto.response;

import lombok.Data;
import java.util.Collection;

@Data
public class BookDetailsResponseDto {

    private Long id;
    private String title;
    private String summary;
    private String author;
    private String publisher;
    private int year;
    private float score;
    private Collection<ReviewResponseDto> reviews;

}
