package master.cloud.apps.dto.response;

import lombok.Data;

@Data
public class ReviewUserResponseDto {

    private String nick;
    private String email;

}
