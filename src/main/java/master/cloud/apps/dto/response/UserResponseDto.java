package master.cloud.apps.dto.response;

import lombok.Data;

@Data
public class UserResponseDto {

    private Long id;
    private String nick;
    private String email;

}
