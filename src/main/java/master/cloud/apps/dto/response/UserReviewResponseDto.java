package master.cloud.apps.dto.response;

import lombok.Data;

@Data
public class UserReviewResponseDto {

    private Long id;
    private String review;
    private float rate;
    private Long bookId;

}
