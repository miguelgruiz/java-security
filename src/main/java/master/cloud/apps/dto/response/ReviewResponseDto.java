package master.cloud.apps.dto.response;

import lombok.Data;

@Data
public class ReviewResponseDto {

    private Long id;
    private ReviewUserResponseDto user;
    private String review;
    private float rate;

}
