package master.cloud.apps.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
public class ReviewRequestDto {

    @NotBlank(message = "Nick is mandatory")
    private String nick;
    @NotBlank(message = "Comment is mandatory")
    private String review;
    @Min(value = 0, message = "Rate must be equals or greater than 0")
    @Max(value = 5, message = "Rate must be equals or less than 5")
    private float rate;

}
