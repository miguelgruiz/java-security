package master.cloud.apps.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateBookRequestDto {

    private String title;
    private String summary;
    private String author;
    private String publisher;
    private int year;

}
