package master.cloud.apps.controller;

import master.cloud.apps.dto.request.BookRequestDto;
import master.cloud.apps.dto.request.ReviewRequestDto;
import master.cloud.apps.dto.request.UpdateBookRequestDto;
import master.cloud.apps.dto.response.BookResponseDto;
import master.cloud.apps.dto.response.BookDetailsResponseDto;
import master.cloud.apps.dto.response.ReviewResponseDto;

import master.cloud.apps.service.BookService;
import master.cloud.apps.service.ReviewService;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v0/books")
public class RestBookController {

    @Autowired
    private BookService bookService;

    @Autowired
    private ReviewService reviewService;

    @GetMapping("/")
    public Collection<BookResponseDto> getBooks() {
        return this.bookService.findAll();
    }

    @GetMapping("/{id}")
    public BookDetailsResponseDto getBook(@PathVariable long id) {
        return this.bookService.findById(id);
    }

    @PostMapping("/")
    public BookDetailsResponseDto createBook(@RequestBody BookRequestDto bookRequestDto) {
        return this.bookService.save(bookRequestDto);
    }

    @PutMapping("/{id}")
    public BookDetailsResponseDto updateBook(@PathVariable long id, @RequestBody UpdateBookRequestDto updateBookRequestDto) {
        return this.bookService.update(id, updateBookRequestDto);
    }

    @DeleteMapping("/{id}")
    public BookDetailsResponseDto deleteBook(@PathVariable long id) {
        return this.bookService.delete(id);
    }

    @PostMapping("/{id}/reviews/")
    public ReviewResponseDto addBookReview(@PathVariable long id, @RequestBody ReviewRequestDto reviewRequestDto) {
        return bookService.addReview(id, reviewRequestDto);
    }

    @DeleteMapping("/{idBook}/reviews/{idReview}")
    public ReviewResponseDto deleteBookReview(@PathVariable long idBook, @PathVariable long idReview) {
        return reviewService.delete(idBook, idReview);
    }

}
