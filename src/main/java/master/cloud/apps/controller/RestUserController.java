package master.cloud.apps.controller;

import master.cloud.apps.dto.request.UpdateUserEmailRequestDto;
import master.cloud.apps.dto.request.UserRequestDto;
import master.cloud.apps.dto.response.UserResponseDto;
import master.cloud.apps.dto.response.UserReviewResponseDto;
import master.cloud.apps.service.ReviewService;
import master.cloud.apps.service.UserService;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v0/users")
public class RestUserController {

    @Autowired
    UserService userService;

    @Autowired
    ReviewService reviewService;

    @GetMapping("/")
    public Collection<UserResponseDto> getUsers() {
        return this.userService.findAll();
    }

    @GetMapping("/{id}")
    public UserResponseDto getUserById(@PathVariable Long id) {
        return this.userService.findById(id);
    }

    @PostMapping("/")
    public UserResponseDto createUser(@RequestBody UserRequestDto userRequestDto) {
        return this.userService.save(userRequestDto);
    }

    @PutMapping("/{id}")
    public UserResponseDto updateEmail(@PathVariable long id, @RequestBody UpdateUserEmailRequestDto updateUserEmailRequestDto) {
        return this.userService.updateEmail(id, updateUserEmailRequestDto);
    }

    @DeleteMapping("/{id}")
    public UserResponseDto deleteUser(@PathVariable long id) {
        return this.userService.delete(id);
    }

    @GetMapping("/{id}/reviews")
    public Collection<UserReviewResponseDto> getUserReviews(@PathVariable long id) {
        return this.reviewService.getReviewsFromUser(id);
    }

}