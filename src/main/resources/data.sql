-- Saving books
INSERT INTO books (title, summary, author, publisher, year) VALUES ("Book 1 title", "Book 1 summary","Book 1 author","Book 1 publisher",1999);
INSERT INTO books (title, summary, author, publisher, year) VALUES ("Book 2 title", "Book 2 summary","Book 2 author","Book 2 publisher",2012);

-- saving users
INSERT INTO users (email, nick, password) VALUES('user1@email.es', 'user1','pass1');
INSERT INTO users (email, nick, password) VALUES('user2@email.es', 'user2','pass2');

-- saving reviews
INSERT INTO reviews (review, rate,  book_id, user_id) VALUES ("Book 2 review 1", 3, 2, 1);
INSERT INTO reviews (review, rate, book_id, user_id) VALUES ("Book 2 review 2", 4, 2, 1);